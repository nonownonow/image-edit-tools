# build 순서

## build a ImageEditor

npm run build:lib --workspace ./packages/react-filerobot-image-editor

## build a server

npm run build --workspace './packages/ide'

## start a server

npm run start --workspace './packages/ide'

## 이미지 자르기 툴 demo

https://nonownonow.github.io/crop/
