import React, { useState } from 'react';
import FilerobotImageEditor, {
  TABS,
  TOOLS,
} from 'react-filerobot-image-editor';
import './ide.css';
import 'common-assets/css/theme.css';
export function Ide() {
  const [isImgEditorShown, setIsImgEditorShown] = useState(false);

  const openImgEditor = () => {
    setIsImgEditorShown(true);
  };

  const closeImgEditor = () => {
    setIsImgEditorShown(false);
  };

  const [imgData, setImgData] = useState('/crop/assets/data/dogs.jpg');
  const onChangeFileHandler = (e) => {
    console.log(e);
    const file = e.target.files?.[0];

    if (file) {
      const img = new Image();
      img.src = URL.createObjectURL(file);
      img.onload = () => {
        setImgData(img);
      };
    }
  };
  const downloadImage = (imageData, filename = 'download.png') => {
    const link = document.createElement('a');
    link.href = imageData.imageBase64;
    link.download = filename;
    document.body.appendChild(link); // Needed for Firefox
    link.click();
    document.body.removeChild(link);
  };

  return (
    <FilerobotImageEditor
      onChangeFileHandler={onChangeFileHandler}
      source={imgData}
      onSave={(editedImageObject, designState) =>
        downloadImage(editedImageObject)
      }
      onClose={closeImgEditor}
      annotationsCommon={{
        fill: '#ff0000',
      }}
      Text={{ text: 'Filerobot...' }}
      Rotate={{ angle: 90, componentType: 'slider' }}
      /*    Crop={{
        presetsItems: [
          {
            titleKey: 'classicTv',
            descriptionKey: '4:3',
            ratio: 4 / 3,
            // icon: CropClassicTv, // optional, CropClassicTv is a React Function component. Possible (React Function component, string or HTML Element)
          },
          {
            titleKey: 'cinemascope',
            descriptionKey: '21:9',
            ratio: 21 / 9,
            // icon: CropCinemaScope, // optional, CropCinemaScope is a React Function component.  Possible (React Function component, string or HTML Element)
          },
        ],
        presetsFolders: [
          {
            titleKey: 'socialMedia', // will be translated into Social Media as backend contains this translation key
            // icon: Social, // optional, Social is a React Function component. Possible (React Function component, string or HTML Element)
            groups: [
              {
                titleKey: 'facebook',
                items: [
                  {
                    titleKey: 'profile',
                    width: 180,
                    height: 180,
                    descriptionKey: 'fbProfileSize',
                  },
                  {
                    titleKey: 'coverPhoto',
                    width: 820,
                    height: 312,
                    descriptionKey: 'fbCoverPhotoSize',
                  },
                ],
              },
            ],
          },
        ],
      }} */
      tabsIds={[TABS.ADJUST, TABS.FILTERS]} // or {['Adjust', 'Annotate', 'Watermark']}
      defaultTabId={TABS.ADJUST} // or 'Annotate'
      defaultToolId={TOOLS.CROP} // or 'Text'
    />
  );
}
