import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import 'destyle.css';
import reportWebVitals from './reportWebVitals';
import { RouterProvider, createBrowserRouter } from 'react-router-dom';
import { Ide } from './routes/ide/ide';
//@ts-ignore
import { ImageEditorStage } from 'react-filerobot-image-editor/lib/components/App';
declare module 'react' {
  function forwardRef<T, P = {}>(
    render: (props: P, ref: React.Ref<T>) => React.ReactElement | null,
  ): (props: P & React.RefAttributes<T>) => React.ReactElement | null;
}

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement,
);

const router = createBrowserRouter(
  [
    {
      path: '/',
      element: <Ide />,
      children: [{ index: true, element: <ImageEditorStage /> }],
    },
  ],
  { basename: '/crop' },
);
root.render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>,
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
