const handleImageScale = (data: HTMLImageElement) => {
  const IMAGE_SIZE = 500;
  const UPLOAD_IMAGE_SIZE = 1024;
  let w = data.naturalWidth;
  let h = data.naturalHeight;
  let scale;
  let uploadScale;
  if (h < w) {
    scale = IMAGE_SIZE / h;
    if (h * scale > 1333) {
      scale = 1333 / h;
    }
    uploadScale = UPLOAD_IMAGE_SIZE / w;
  } else {
    scale = IMAGE_SIZE / w;
    if (w * scale > 1333) {
      scale = 1333 / w;
    }
    uploadScale = UPLOAD_IMAGE_SIZE / h;
  }
  return { height: h, width: w, scale, uploadScale };
};

async function resizeImage(
  image: HTMLImageElement,
  width: number,
  height: number
) {
  return new Promise<HTMLImageElement>(async (resolve, reject) => {
    const canvas = document.createElement("canvas");
    const ctx = canvas.getContext("2d");
    canvas.width = width;
    canvas.height = height;
    ctx?.drawImage(image, 0, 0, width, height);
    const blob = await new Promise<Blob | null>((innerResolve) => {
      canvas.toBlob((innerBlob) => {
        innerResolve(innerBlob);
      });
    });
    if (!blob) {
      reject(new Error("이미지 크기를 조정할 수 없습니다."));
      return;
    }
    const newImage = new Image();
    newImage.src = URL.createObjectURL(blob);
    newImage.onload = () => {
      // URL.revokeObjectURL(newImage.src);
      resolve(newImage);
    };
    newImage.onerror = (error) => {
      reject(error);
    };
  });
}

function mergeImages(
  images: HTMLImageElement[],
  callback: (e: HTMLImageElement) => void
) {
  if (images.length === 0) return;
  var canvas = document.createElement("canvas");
  var ctx = canvas.getContext("2d");
  canvas.width = images[0].width;
  canvas.height = images[0].height;

  images.forEach((img, index) => {
    if (!ctx) return;
    ctx.drawImage(img, 0, 0);
  });

  canvas.toBlob((blob) => {
    if (!blob) return;
    const newImg = new Image();
    newImg.src = URL.createObjectURL(blob);
    callback(newImg);
  }, "image/png");
}
export { handleImageScale, resizeImage, mergeImages };
