import type { ComponentPropsWithRef } from "react";
import { forwardRef } from "react";
import { VALUE } from "fxtsx/VALUE/VALUE";

export type ButtonProps = VALUE &
  (
    | ({ href: string } & ComponentPropsWithRef<"a">)
    | ({ href?: never } & ComponentPropsWithRef<"button">)
  );

export const Button = forwardRef<
  HTMLButtonElement | HTMLAnchorElement,
  ButtonProps
>((props, ref) => (
  <VALUE
    data-fx-button
    Root={props.href ? "a" : "button"}
    ref={ref}
    {...props}
  />
));
