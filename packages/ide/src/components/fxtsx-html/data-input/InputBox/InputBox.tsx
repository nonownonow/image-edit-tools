import type { ComponentPropsWithoutRef, ReactNode } from "react";
import React, { forwardRef } from "react";
import "./InputBox.css";
import { separateProps } from "fxtsx/util/separateProps";

export type InputBoxProps = {
  $unCheckedMark?: ReactNode;
  $checkedMark?: ReactNode;
  $type?: "checkbox" | "radio";
} & Omit<ComponentPropsWithoutRef<"input">, "type">;

export const InputBox = forwardRef<HTMLInputElement, InputBoxProps>(
  (props, ref) => {
    const [rootProps, restProps] = separateProps(props);
    const {
      $checkedMark = "[V]",
      $unCheckedMark = "[ ]",
      $type = "checkbox",
      children,
      ...inputBoxProps
    } = restProps;
    return (
      <label data-fx-input-box={$type} {...rootProps}>
        <input type={$type} {...inputBoxProps} ref={ref} />
        <span>{$unCheckedMark}</span>
        <span>{$checkedMark}</span>
      </label>
    );
  }
);
