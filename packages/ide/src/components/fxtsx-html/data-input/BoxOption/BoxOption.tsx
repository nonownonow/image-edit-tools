import type { ComponentPropsWithoutRef } from "react";
import React, { forwardRef } from "react";
import { VALUE } from "fxtsx/VALUE/VALUE";
import { htmlChildren } from "fxtsx/util/util";
import "./BoxOption.css";
import { separateProps } from "fxtsx/util/separateProps";
import { InputBox } from "fxtsx-html/data-input/InputBox/InputBox";

export type BoxOptionProps = VALUE & ComponentPropsWithoutRef<"input">;

export const BoxOption = forwardRef<HTMLInputElement, BoxOptionProps>(
  (props: BoxOptionProps, ref) => {
    const [rootProps, restProps] = separateProps(props);
    const {
      $data,
      children,
      $valueLabel = children,
      value,
      ...boxOptionProps
    } = restProps;
    return (
      <VALUE
        data-fx-check-option
        Root={"label"}
        {...rootProps}
        $valueLabel={
          <>
            <span {...htmlChildren($valueLabel)} />
            <InputBox value={$data} {...boxOptionProps} ref={ref} />
          </>
        }
      />
    );
  }
);
