import { BoxOption } from "fxtsx-html/data-input/BoxOption/BoxOption";
import type { Meta, StoryObj } from "@storybook/react";

const meta = {
  component: BoxOption,
  tags: ["autodocs"],
} satisfies Meta<typeof BoxOption>;

export default meta;
type Story = StoryObj<typeof meta>;
export const Default: Story = {
  args: {
    $data: "PROGRAMMER",
    $valueLabel: "개발자",
  },
};
