import { SECTIONING } from "fxtsx/SECTIONING/SECTIONING";
import type { ComponentPropsWithoutRef } from "react";
import { forwardRef } from "react";
import { Heading } from "fxtsx-html/sectioning/Heading/Heading";

export type NavProps = SECTIONING & ComponentPropsWithoutRef<"nav">;

export const Nav = forwardRef<HTMLElement, NavProps>((props, ref) => {
  return <SECTIONING Root={"nav"} Heading={Heading} {...props} ref={ref} />;
});
