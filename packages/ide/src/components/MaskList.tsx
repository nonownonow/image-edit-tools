import { useContext, useEffect, useState } from 'react';
import AppContext from './hooks/createContext';
import './MaskList.css';
import { mergeImages } from './helpers/ImageHelper';
export function MaskList() {
  const {
    maskImgs: [maskImgs, setMaskImgs],
    mergedMaskImg: [mergedMaskImg, setMergedMaskImg],
  } = useContext(AppContext)!;
  useEffect(
    () =>
      mergeImages(maskImgs, (mergeImage) => {
        setMergedMaskImg(mergeImage);
      }),
    [maskImgs],
  );
  return (
    <>
      <ul className={'maskList'}>
        {maskImgs.map((mask, i) => {
          if (!mask) return null;
          return (
            <li>
              <img src={mask.src}></img>
            </li>
          );
        })}
        {mergedMaskImg ? (
          <li>
            <img src={mergedMaskImg.src} />
          </li>
        ) : null}
      </ul>
    </>
  );
}
