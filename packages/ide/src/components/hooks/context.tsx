import { Tensor } from "onnxruntime-web";
import React, { useState } from "react";
import { AnnotationProps, modelInputProps, Points } from "../helpers/Interface";
import AppContext, { Mask } from "./createContext";

const AppContextProvider = (props: {
  children: React.ReactElement<any, string | React.JSXElementConstructor<any>>;
}) => {
  const [click, setClick] = useState<modelInputProps | null>(null);
  const [clicks, setClicks] = useState<Array<modelInputProps> | null>(null);
  const [clicksHistory, setClicksHistory] =
    useState<Array<modelInputProps> | null>(null);
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [image, setImage] = useState<HTMLImageElement | null>(null);
  const [prevImage, setPrevImage] = useState<HTMLImageElement | null>(null);
  const [isErasing, setIsErasing] = useState<boolean>(false);
  const [isErased, setIsErased] = useState<boolean>(false);
  const [error, setError] = useState<boolean>(false);
  const [svg, setSVG] = useState<string[] | null>(null);
  const [svgs, setSVGs] = useState<string[][] | null>(null);
  const [allsvg, setAllsvg] = useState<
    { svg: string[]; point_coord: number[] }[] | null
  >(null);
  const [isModelLoaded, setIsModelLoaded] = useState<{
    boxModel: boolean;
    allModel: boolean;
  }>({ boxModel: false, allModel: false });
  const [stickers, setStickers] = useState<HTMLCanvasElement[]>([]);
  const [stickerUrls, setStickerUrls] = useState<string[]>([]);
  const [mask, setMask] = useState<Mask | null>(null);
  const [activeSticker, setActiveSticker] = useState<number>(0);
  const [segmentTypes, setSegmentTypes] = useState<"Box" | "Click" | "All">(
    "Click"
  );
  const [canvasWidth, setCanvasWidth] = useState<number>(0);
  const [canvasHeight, setCanvasHeight] = useState<number>(0);
  const [maskImg, setMaskImg] = useState<HTMLImageElement | null>(null);
  const [maskImgs, setMaskImgs] = useState<HTMLImageElement[]>([]);
  const [mergedMaskImg, setMergedMaskImg] = useState<HTMLImageElement | null>(
    null
  );
  const [finalMaskImg, setFinalMaskImg] = useState<HTMLImageElement | null>(
    null
  );
  const [multiMaskImg, setMultiMaskImg] = useState<HTMLCanvasElement | null>(
    null
  );
  const [maskCanvas, setMaskCanvas] = useState<HTMLCanvasElement | null>(null);
  const [userNegClickBool, setUserNegClickBool] = useState<boolean>(false);
  const [hasNegClicked, setHasNegClicked] = useState<boolean>(false);
  const [stickerTabBool, setStickerTabBool] = useState<boolean>(false);
  const [enableDemo, setEnableDemo] = useState(false);
  const [isMultiMaskMode, setIsMultiMaskMode] = useState<boolean>(false);
  const [isHovering, setIsHovering] = useState<boolean | null>(null);
  const [showLoadingModal, setShowLoadingModal] = useState<boolean>(false);
  const [eraserText, setEraserText] = useState<{
    isErase: boolean;
    isEmbedding: boolean;
  }>({ isErase: false, isEmbedding: false });
  const [didShowAMGAnimation, setDidShowAMGAnimation] =
    useState<boolean>(false);
  const [predMask, setPredMask] = useState<Tensor | null>(null);
  const [predMasks, setPredMasks] = useState<Tensor[] | null>(null);
  const [predMasksHistory, setPredMasksHistory] = useState<Tensor[] | null>(
    null
  );
  const [isAllAnimationDone, setIsAllAnimationDone] = useState<boolean>(false);
  const [isToolBarUpload, setIsToolBarUpload] = useState<boolean>(false);
  const apiBaseUrl = process.env.REACT_APP_API_URL;
  const url = {
    segment: `${apiBaseUrl}/segment`,
    segmentList: `${apiBaseUrl}/segment/list`,
    segmentSave: `${apiBaseUrl}/segment/save`,
    cdnImage: process.env.REACT_APP_CDN_IMG_URL as string,
  } as const;
  const [tensor, setTensor] = useState<Tensor | null>(null);
  const [hasClicked, setHasClicked] = useState<boolean>(false);
  const [annotations, setAnnotations] = useState<Array<AnnotationProps>>([]);
  const [newAnnotation, setNewAnnotation] = useState<Array<AnnotationProps>>(
    []
  );
  const [points, setPoints] = useState<Points | undefined>(undefined);
  const handleResetState = () => {
    setMaskImg(null);
    setMaskImgs([]);
    setMergedMaskImg(null);
    setHasClicked(false);
    setClick(null);
    setClicks(null);
    setSVG(null);
    setSVGs(null);
    setAllsvg(null);
    setTensor(null);
    setImage(null);
    setPrevImage(null);
    setPredMask(null);
    setIsErased(false);
    setShowLoadingModal(false);
    setIsModelLoaded({ boxModel: false, allModel: false });
    setSegmentTypes("Click");
    setIsLoading(false);
    setIsMultiMaskMode(false);
    setIsHovering(null);
    setPredMasks(null);
  };

  const handleResetInteraction = (forceFullReset?: boolean) => {
    setSVG(null);
    setSVGs(null);
    setClick(null);
    setClicks(null);
    setAnnotations([]);
    setNewAnnotation([]);
    setClicksHistory(null);
    setMaskImg(null);
    setUserNegClickBool(false);
    setIsHovering(null);
    setPredMask(null);
    setPredMasks(null);
    setPredMasksHistory(null);
    setIsLoading(false);
    setPoints(undefined);
    if (segmentTypes === "Click" && !forceFullReset) {
      if (!isMultiMaskMode) {
        setHasClicked(false);
      }
    } else {
      setHasClicked(false);
      setIsMultiMaskMode(false);
    }
  };
  return (
    <AppContext.Provider
      value={{
        tensor: [tensor, setTensor],
        hasClicked: [hasClicked, setHasClicked],
        click: [click, setClick],
        clicks: [clicks, setClicks],
        clicksHistory: [clicksHistory, setClicksHistory],
        image: [image, setImage],
        prevImage: [prevImage, setPrevImage],
        error: [error, setError],
        svg: [svg, setSVG],
        svgs: [svgs, setSVGs],
        allsvg: [allsvg, setAllsvg],
        stickers: [stickers, setStickers],
        stickerUrls: [stickerUrls, setStickerUrls],
        mask: [mask, setMask],
        mergedMaskImg: [mergedMaskImg, setMergedMaskImg],
        activeSticker: [activeSticker, setActiveSticker],
        isModelLoaded: [isModelLoaded, setIsModelLoaded],
        segmentTypes: [segmentTypes, setSegmentTypes],
        isLoading: [isLoading, setIsLoading],
        isErasing: [isErasing, setIsErasing],
        isErased: [isErased, setIsErased],
        canvasWidth: [canvasWidth, setCanvasWidth],
        canvasHeight: [canvasHeight, setCanvasHeight],
        maskImg: [maskImg, setMaskImg],
        maskImgs: [maskImgs, setMaskImgs],
        finalMaskImg: [finalMaskImg, setFinalMaskImg],
        multiMaskImg: [multiMaskImg, setMultiMaskImg],
        maskCanvas: [maskCanvas, setMaskCanvas],
        userNegClickBool: [userNegClickBool, setUserNegClickBool],
        hasNegClicked: [hasNegClicked, setHasNegClicked],
        stickerTabBool: [stickerTabBool, setStickerTabBool],
        enableDemo: [enableDemo, setEnableDemo],
        isMultiMaskMode: [isMultiMaskMode, setIsMultiMaskMode],
        isHovering: [isHovering, setIsHovering],
        showLoadingModal: [showLoadingModal, setShowLoadingModal],
        eraserText: [eraserText, setEraserText],
        didShowAMGAnimation: [didShowAMGAnimation, setDidShowAMGAnimation],
        predMask: [predMask, setPredMask],
        predMasks: [predMasks, setPredMasks],
        predMasksHistory: [predMasksHistory, setPredMasksHistory],
        annotations: [annotations, setAnnotations],
        newAnnotation: [newAnnotation, setNewAnnotation],
        isAllAnimationDone: [isAllAnimationDone, setIsAllAnimationDone],
        isToolBarUpload: [isToolBarUpload, setIsToolBarUpload],
        points: [points, setPoints],
        url,
        handleResetState,
        handleResetInteraction,
      }}
    >
      {props.children}
    </AppContext.Provider>
  );
};

export default AppContextProvider;
