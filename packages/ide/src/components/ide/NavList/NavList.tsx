import { Ul } from "fxtsx-html/data-structure/Ul/Ul";
import { COLLECTION } from "fxtsx/COLLECTION/COLLECTION";

export type NavData = {
  id: string;
  name: string;
  href: string;
};

export function NavList(props: COLLECTION<NavData>) {
  const { $data } = props;
  return (
    <Ul
      {...props}
      $data={$data}
      $itemFormat={(navData) => <a href={navData.href}>{navData.name}</a>}
    />
  );
}
