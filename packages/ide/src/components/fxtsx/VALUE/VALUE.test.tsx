import type { RenderResult } from "@testing-library/react";
import { render } from "@testing-library/react";
import { createRef } from "react";
import { VALUE } from "fxtsx/VALUE/VALUE";
import { Default } from "fxtsx/VALUE/VALUE.stories";
import { fxtsxTest2 } from "fxtsx/util/fxtsxTest2";

describe("구조", () => {
  let renderResult: RenderResult;
  const callBack = {};
  let rootEl: ChildNode | null;
  beforeEach(() => {
    renderResult = render(<VALUE {...callBack} {...(Default.args as VALUE)} />);
    rootEl = renderResult.container.firstChild;
  });
  fxtsxTest2(VALUE, "data-fx-value");
  test("스냅샷", () => {
    const { asFragment } = renderResult;
    expect(asFragment()).toMatchInlineSnapshot(`
      <DocumentFragment>
        <div
          data-fx-value="true"
        >
          개발자
        </div>
      </DocumentFragment>
    `);
  });
});
