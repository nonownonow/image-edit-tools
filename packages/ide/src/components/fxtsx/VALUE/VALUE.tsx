import { forwardRef, Ref, type FC, type ReactNode } from "react";
import { htmlChildren } from "fxtsx/util/util";
import { separateProps } from "fxtsx/util/separateProps";
import { Div } from "fxtsx/Identity/Div";

export type VALUEProps = VALUE &
  VALUECallback & {
    children?: ReactNode;
  };

export type VALUE = {
  $data?: any;
  $valueLabel?: ReactNode;
};
export interface VALUECallback {
  Root?: string | FC<any>;
}

export const VALUE = forwardRef((props: VALUEProps, ref: Ref<any>) => {
  const [rootProps, restProps] = separateProps(props);
  const {
    Root = Div,
    $data,
    children,
    $valueLabel = children,
    ...valueProps
  } = restProps;
  return (
    <Root
      data-fx-value
      {...rootProps}
      {...valueProps}
      {...htmlChildren($valueLabel || $data)}
      ref={ref}
    />
  );
});
