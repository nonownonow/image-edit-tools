import { FC, forwardRef, ReactNode, Ref } from "react";
import { COLLECTION } from "fxtsx/COLLECTION/COLLECTION";
import { DICTIONARY } from "fxtsx/DICTIONARY/DICTIONARY";
import { identity } from "@fxts/core";
import type { DicData } from "fxtsx/fxtsx.type";
import { separateProps } from "fxtsx/util/separateProps";

export type DATAFRAMEProps<Dic extends DicData> = DATAFRAME<Dic> &
  DATAFRAMECallback<Dic>;
export interface DATAFRAME<Dic extends DicData>
  extends Omit<COLLECTION<Dic>, "$itemFormat">,
    Omit<DICTIONARY<Dic>, "$data"> {
  $itemFormat?: (value: ReactNode, index: number) => any;
}
export interface DATAFRAMECallback<Dic extends DicData> {
  Root?: FC<COLLECTION<Dic>>;
  Dictionary?: FC<any>;
}
export const DATAFRAME = forwardRef(function <Dic extends DicData>(
  props: DATAFRAMEProps<Dic>,
  ref: Ref<any>
) {
  const [rootProps, restProps] = separateProps(props);

  const {
    $data: collection = [],
    Root = COLLECTION,
    Dictionary = DICTIONARY,
    $itemFormat = identity,
    $keyFormat = identity,
    $keyFormats,
    $valueFormat = identity,
    $valueFormats,
    ...dataFrameProps
  } = restProps;
  return (
    <Root
      data-fx-dataframe
      $data={collection}
      $itemFormat={(item, index) =>
        // itemFormat 은 Item 으로 렌더링 되기 전, item 을 포멧팅한다.
        $itemFormat(
          <Dictionary
            $data={item}
            $keyFormat={$keyFormat}
            $keyFormats={$keyFormats}
            $valueFormat={$valueFormat}
            $valueFormats={$valueFormats}
          />,
          index
        )
      }
      ref={ref}
      {...rootProps}
      {...dataFrameProps}
    />
  );
});
