import React, { forwardRef } from "react";
import { entries, map, pipe } from "@fxts/core";
import { kebabCase } from "lodash";
import { Props } from "fxtsx/fxtsx.type";

export const Div = function Div(props: Props) {
  const toNativeProps = pipe(
    entries(props),
    map(([k, v]) => [
      ["className", "dangerouslySetInnerHTML"].includes(k)
        ? k
        : kebabCase(k as string),
      v,
    ]),
    Object.fromEntries
  );
  return <div {...toNativeProps} />;
};
