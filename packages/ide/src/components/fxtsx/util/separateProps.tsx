import type { ComponentProps, ForwardedRef, ReactElement } from "react";
import { map, partition, pipe, some, toArray } from "@fxts/core";
import { Props, RestProps, RootProps } from "fxtsx/fxtsx.type";
const defaultPropKeys = ["className", "tabIndex", "style", "hidden", /data-.+/];
export function separateProps<P extends Props>(
  props: P,
  rootPropsKeys: (string | RegExp)[] = defaultPropKeys
): [RootProps, RestProps<P>] {
  const rootPropsAndOtherProps = ([key]: [string, unknown]) =>
    pipe(
      rootPropsKeys,
      some((matcher) => RegExp(matcher).test(key))
    );

  return pipe(
    Object.entries(props),
    partition(rootPropsAndOtherProps),
    map(Object.fromEntries),
    toArray
  ) as [RootProps, RestProps<P>];
}

export interface FXTSXRenderFunction<
  T,
  P,
  ROOT = RootProps,
  REST = RestProps<P>
> {
  (rootProps: ROOT, restProps: REST, ref: ForwardedRef<T>): ReactElement | null;
}

export const rootProps: RootProps = {
  id: "my-id",
  className: "my-class",
  tabIndex: 0,
  style: {
    fontSize: "1rem",
  },
  "data-test": "my-data-test",
};
export const anyPropsWithRootProps = {
  ...rootProps,
  any: "my-any-props",
};
