import { FC, forwardRef } from "react";
import { createElement } from "react";
import { Default } from "fxtsx/Identity/Default";
import { separateProps } from "fxtsx/util/separateProps";
import { Div } from "fxtsx/Identity/Div";

export type ENTRYProps = ENTRY & ENTRYCallback;

export interface ENTRY {
  /**
   * 엔트리 형태 = [key, value]
   * */
  $data: [string, any];
  /**
   * 키의 라벨
   * */
  $keyLabel?: string;
  /**
   * 값의 라벨
   * */
  $valueLabel?: string;
}
export interface ENTRYCallback {
  Root?: string | FC<any>;
  Key?: string | FC<any>;
  Value?: string | FC<any>;
}

/**
 * 엔트리의 구조를 구현한 고차 컴포넌트
 * */
export const ENTRY = forwardRef(function ENTRY(props: ENTRYProps, ref) {
  const [rootProps, restProps] = separateProps(props);
  const {
    Root = Div,
    Key = Div,
    Value = Div,
    $data: [$key, $value],
    $keyLabel,
    $valueLabel,
    ...entryProps
  } = restProps;
  return createElement(
    Root,
    {
      "data-fx-entry": $key,
      ref,
      ...rootProps,
    },
    [
      createElement(Key, { "data-key": $key, key: 0 }, $keyLabel || $key),
      createElement(
        Value,
        {
          "data-value": $value,
          key: 1,
          ...entryProps,
        },
        $valueLabel || $value
      ),
    ]
  );
});
