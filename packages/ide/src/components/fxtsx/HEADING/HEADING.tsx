import { FC, forwardRef, ReactNode } from "react";
import { createElement } from "react";
import { VALUE } from "fxtsx/VALUE/VALUE";
import { separateProps } from "fxtsx/util/separateProps";
import { Div } from "fxtsx/Identity/Div";

export type HEADINGProps = HEADING &
  HEADINGCallback & {
    /**
     * =subTitle 프로퍼티
     */
    children?: ReactNode;
  };
//$접두사를 이용해서 fxtsx 프로퍼티의 고유한 이름을 보장한다.
export interface HEADING {
  /**
   * 제목
   */
  $title?: string;
  /**
   * 제목의 레벨
   */
  $level: number;
  /**
   * 부제목
   */
  $subTitle?: ReactNode;
}

export type HEADINGCallback = {
  /**
   * 제목을 구현하는 컨포넌트
   */
  Root?: string | FC<any>;
  /**
   * 제목 그룹을 구현하는 컨포넌트
   */
  HeadingGroup?: string | FC<any>;
};

/**
 * 헤딩(h1 ~ h6)태그와 대응하는 컴포넌트 구현을 위한 인터페이스
 * */
export const HEADING = forwardRef(function (props: HEADINGProps, ref: any) {
  const [rootProps, restProps] = separateProps(props);
  const {
    Root = Div,
    HeadingGroup = Div,
    children,
    $subTitle,
    $level,
    ...headingProps
  } = restProps;

  return $subTitle
    ? createElement(
        HeadingGroup,
        {
          "data-fx-heading": true,
          "data-heading-level": $level,
          ...rootProps,
        },
        <>
          <Root
            data-fx-heading
            {...headingProps}
            key={"0"}
            ref={ref}
            $level={$level}
          />
          {$subTitle}
        </>
      )
    : createElement(Root, {
        "data-fx-heading": true,
        "data-heading-level": $level,
        $level: $level,
        ref,
        ...rootProps,
        ...headingProps,
      });
});
