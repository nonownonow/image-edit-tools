const getDimensionsMinimalRatio = (
  firstWidth,
  firstHeight,
  secondWidth,
  secondHeight,
) => {
  const widthScale = firstWidth / secondWidth;
  const heightScale = firstHeight / secondHeight;

  return Math.min(widthScale, heightScale) || 1; //화면에 다 들어가야 하므로 최소 scale로 적용
};

export default getDimensionsMinimalRatio;
