/** External Dependencies */
import React, { useEffect, useRef, useState } from 'react';
import { Ellipse, Image, Rect, Transformer } from 'react-konva';
import Konva from 'konva';

/** Internal Dependencies */
import { useStore } from 'hooks';
import { SET_CROP, SET_FEEDBACK } from 'actions';
import {
  CUSTOM_CROP,
  ELLIPSE_CROP,
  FEEDBACK_STATUSES,
  ORIGINAL_CROP,
  TOOLS_IDS,
} from 'utils/constants';
import { boundDragging, boundResizing } from './cropAreaBounding';
import toPrecisedFloat from 'utils/toPrecisedFloat';

let isFirstRenderCropUpdated = false;
/* 
  크랍 툴을 사용할 때, 이미지를 크랍할 수 있도록 도와주는 컴포넌트입니다.
*/
const CropTransformer = () => {
  const store = useStore();
  const {
    dispatch,
    theme,
    designLayer,
    originalImage,
    shownImageDimensions,
    adjustments: { crop = {}, isFlippedX, isFlippedY } = {},
    resize = {},
    config,
    zoom,
    t,
  } = store;
  const cropShapeRef = useRef();
  const cropTransformerRef = useRef();
  const tmpImgNodeRef = useRef();
  const shownImageDimensionsRef = useRef();
  const cropConfig = config[TOOLS_IDS.CROP]; // crop의 기본 설정을 가져옵니다.
  const cropRatio = crop.ratio || cropConfig.ratio;
  const isCustom = cropRatio === CUSTOM_CROP;
  const isEllipse = cropRatio === ELLIPSE_CROP;

  const saveCrop = ({ width, height, x, y }, noHistory) => {
    const newCrop = {
      x: isFlippedX ? shownImageDimensions.width - x - width : x,
      y: isFlippedY ? shownImageDimensions.height - y - height : y,
      width,
      height,
    };

    const isOldCropBiggerThanResize =
      crop.width >= resize.width && crop.height >= resize.height;
    if (
      resize.width &&
      resize.height &&
      (width < resize.width || height < resize.height) &&
      isOldCropBiggerThanResize
    ) {
      dispatch({
        type: SET_FEEDBACK,
        payload: {
          feedback: {
            message: t('cropSizeLowerThanResizedWarning'),
            status: FEEDBACK_STATUSES.WARNING,
          },
        },
      });
    }

    dispatch({
      type: SET_CROP,
      payload: {
        ...crop,
        ...newCrop,
        dismissHistory: noHistory,
      },
    });
  };
  useEffect(() => {
    dispatch({
      type: SET_CROP,
      payload: {
        x: 0,
        y: 0,
        // imageX: 0,
        // imageY: 0,
        ratio: 'original',
        // width: shownImageDimensions.width,
        // height: shownImageDimensions.height,
      },
    });
  }, [shownImageDimensions]);
  const saveBoundedCropWithLatestConfig = (cropWidth, cropHeight, x, y) => {
    if (cropTransformerRef.current && cropShapeRef.current) {
      cropTransformerRef.current.nodes([cropShapeRef.current]);
    }

    const imageDimensions = shownImageDimensionsRef.current;

    const attrs = {
      width: cropWidth,
      height: cropHeight,
      x: x ?? crop.x ?? 0,
      y: y ?? crop.y ?? 0,
    };

    saveCrop(
      boundResizing(
        attrs,
        attrs,
        { ...imageDimensions, abstractX: 0, abstractY: 0 },
        isCustom || isEllipse
          ? false
          : getProperCropRatio(cropRatio, originalImage),
        cropConfig,
      ),
      true,
    );
  };

  useEffect(() => {
    if (designLayer && cropTransformerRef.current && cropShapeRef.current) {
      if (tmpImgNodeRef.current) {
        tmpImgNodeRef.current.cache();
      }
      cropTransformerRef.current.nodes([cropShapeRef.current]);
    }

    return () => {
      if (tmpImgNodeRef.current) {
        tmpImgNodeRef.current.clearCache();
      }
    };
  }, [designLayer, originalImage, shownImageDimensions]);

  useEffect(() => {
    if (shownImageDimensionsRef.current) {
      if (typeof cropRatio !== 'string') {
        const imageDimensions = shownImageDimensionsRef.current;
        let cropHeight = imageDimensions.height;
        let cropWidth = imageDimensions.height * cropRatio;
        if (cropWidth > imageDimensions.width) {
          cropWidth = imageDimensions.width;
          cropHeight = imageDimensions.width / cropRatio;
        }
        saveBoundedCropWithLatestConfig(
          shownImageDimensions.width,
          shownImageDimensions.height,
          shownImageDimensions.width / 2 - cropWidth / 2,
          shownImageDimensions.height / 2 - cropHeight / 2,
        );
      } else {
        saveBoundedCropWithLatestConfig(
          shownImageDimensions.width,
          shownImageDimensions.height,
          0,
          0,
        );
      }
    }
  }, [cropRatio]);

  useEffect(() => {
    if (
      cropTransformerRef.current &&
      cropShapeRef.current &&
      shownImageDimensionsRef.current &&
      crop.width &&
      crop.height
    ) {
      saveBoundedCropWithLatestConfig(crop.width, crop.height);
    }
  }, [cropConfig, shownImageDimensions.width, shownImageDimensions.height]);

  useEffect(() => {
    if (shownImageDimensions) {
      shownImageDimensionsRef.current = shownImageDimensions;
      // Used to fill in the missing crop at the first render incase of custom crop ratio provided from config so we need to apply it
      if (
        !isFirstRenderCropUpdated &&
        cropRatio &&
        shownImageDimensions.x &&
        shownImageDimensions.width
      ) {
        saveBoundedCropWithLatestConfig(
          crop.width ?? shownImageDimensions.width,
          crop.height ?? shownImageDimensions.height,
        );
        isFirstRenderCropUpdated = true;
      }
    }
  }, [shownImageDimensions]);

  if (!designLayer) {
    return null;
  }

  const enabledAnchors =
    isCustom || isEllipse
      ? undefined
      : ['top-left', 'bottom-left', 'top-right', 'bottom-right'];

  const saveCropFromEvent = (e, noHistory = false) => {
    const designImage = designLayer.children[0];
    const tmpImgNode = tmpImgNodeRef.current;
    dispatch({
      type: SET_CROP,
      payload: {
        dismissHistory: noHistory,
        imageX: tmpImgNode.attrs.x,
        imageY: tmpImgNode.attrs.y,
      },
    });
  };

  const limitDragging = (e) => {
    const tmpImgNode = tmpImgNodeRef.current.attrs;
    const cropShape = cropShapeRef.current.attrs;
    const newDimensions = {
      x: tmpImgNode.x - tmpImgNode.offsetX * tmpImgNode.scaleX,
      y: tmpImgNode.y - tmpImgNode.offsetY * tmpImgNode.scaleY,
      width: tmpImgNode.width,
      height: tmpImgNode.height,
    };
    const dimension = boundDragging(newDimensions, cropShape);
    const designLayerImage = designLayer.children[0];
    designLayerImage.x(dimension.x + tmpImgNode.offsetX * tmpImgNode.scaleX);
    designLayerImage.y(dimension.y + tmpImgNode.offsetY * tmpImgNode.scaleY);
    tmpImgNodeRef.current.x(
      dimension.x + tmpImgNode.offsetX * tmpImgNode.scaleX,
    );
    tmpImgNodeRef.current.y(
      dimension.y + tmpImgNode.offsetY * tmpImgNode.scaleY,
    );
  };

  const cropShapeProps = {
    ref: cropShapeRef,
    fill: '#FFFFFF',
    scaleX: 1,
    scaleY: 1,
    onTransformEnd: saveCropFromEvent,
    draggable: false,
  };

  const cropShapeDimension = getCropShapeDimension(
    shownImageDimensions,
    crop,
    isFlippedX,
    isFlippedY,
    isCustom,
    isEllipse,
    cropConfig,
    getProperCropRatio(cropRatio, originalImage),
  );
  // ALT is used to center scaling

  const imageScaledDimensions = getImageScaledDimensions(
    shownImageDimensions,
    zoom,
    crop,
    isFlippedX,
    isFlippedY,
  );

  // scaleX값이 변했을 때, 여러 x값을 scaleX와 offsetX를 이용하여 최종 결과값을 예측한 후 ImageX를 계산하여 x의 값을 제한한다. 왜냐하면, konva에서는 scaleX값이 변하면, x값에 scale과 offset을 곱해서 반영하기 때문이다.
  const limitedImageX = toPrecisedFloat(
    limitPosition(imageScaledDimensions, cropShapeDimension, true) +
      imageScaledDimensions.offsetX * imageScaledDimensions.scaleX,
  );
  const limitedImageY = toPrecisedFloat(
    limitPosition(imageScaledDimensions, cropShapeDimension, false) +
      imageScaledDimensions.offsetY * imageScaledDimensions.scaleY,
  );

  return (
    <>
      {isEllipse ? (
        <Ellipse
          {...cropShapeProps}
          radiusX={width / 2}
          radiusY={height / 2}
          offset={{
            x: -width / 2,
            y: -height / 2,
          }}
        />
      ) : (
        <Rect {...cropShapeProps} {...cropShapeDimension} />
      )}
      <Image
        image={originalImage}
        {...imageScaledDimensions}
        x={limitedImageX}
        y={limitedImageY}
        filters={[Konva.Filters.Brighten]}
        blurRadius={10}
        brightness={-0.3}
        globalCompositeOperation={'source-out'}
        draggable={true}
        onDragMove={() => {
          limitDragging();
        }}
        onDragEnd={saveCropFromEvent}
        ref={tmpImgNodeRef}
      />
      <Transformer
        centeredScaling={false}
        flipEnabled={false}
        rotateEnabled={false}
        nodes={cropShapeRef.current ? [cropShapeRef.current] : []}
        anchorSize={14}
        anchorCornerRadius={7}
        enabledAnchors={false}
        ignoreStroke={false}
        anchorStroke={theme.palette['accent-primary']}
        anchorFill={theme.palette['access-primary']}
        anchorStrokeWidth={2}
        borderEnabled={crop.ratio === 'original' ? false : true}
        borderStroke={theme.palette['accent-primary']}
        borderStrokeWidth={2}
        borderDash={[4]}
        keepRatio={!isCustom || !isEllipse}
        ref={cropTransformerRef}
      />
    </>
  );
};

export default CropTransformer;

export function limitPosition(
  tmpImgNodeDimensions,
  cropShapeDimensions,
  isAxisX = true,
) {
  const axis = isAxisX ? 'x' : 'y';
  const imgSize = isAxisX ? 'width' : 'height';
  const isInsideIncrease =
    tmpImgNodeDimensions[axis] > cropShapeDimensions[axis];
  const isInsideDecrease =
    tmpImgNodeDimensions[axis] + tmpImgNodeDimensions[imgSize] <
    cropShapeDimensions[axis] + cropShapeDimensions[imgSize];
  const isInside = isInsideIncrease || isInsideDecrease;
  let res = tmpImgNodeDimensions[axis];
  if (isInside) {
    if (isInsideIncrease) {
      res = cropShapeDimensions[axis];
    } else {
      res = -toPrecisedFloat(
        tmpImgNodeDimensions[imgSize] -
          (cropShapeDimensions[axis] + cropShapeDimensions[imgSize]),
      );
    }
  }
  return res;
}

export function getImageScaledDimensions(
  imageDimensions,
  zoom,
  crop,
  isFlippedX,
  isFlippedY,
) {
  const offsetX = imageDimensions.width / 2;
  const offsetY = imageDimensions.height / 2;
  const scaleX = isFlippedX ? -1 : zoom.factor;
  const scaleY = isFlippedY ? -1 : zoom.factor;
  const imageWidth = imageDimensions.width;
  const imageHeight = imageDimensions.height;
  const imageX = isFlippedX ? imageDimensions.width : crop.imageX ?? offsetX;
  const imageY = isFlippedY ? imageDimensions.height : crop.imageY ?? offsetY;

  return {
    x: toPrecisedFloat(imageX - offsetX * scaleX),
    y: toPrecisedFloat(imageY - offsetY * scaleY),
    width: imageWidth * scaleX,
    height: imageHeight * scaleY,
    offsetX,
    offsetY,
    scaleX,
    scaleY,
  };
}

export function getCropShapeDimension(
  imageDimensions,
  crop,
  isFlippedX,
  isFlippedY,
  isCustom,
  isEllipse,
  cropConfig,
  properCropRatio,
) {
  let attrs;
  if (!crop.width && !crop.height) {
    const scaleFactor =
      imageDimensions.scaledBy < 1 ? imageDimensions.scaledBy : 1;
    const unscaledImgDimensions = {
      ...imageDimensions,
      width: imageDimensions.width / scaleFactor,
      height: imageDimensions.height / scaleFactor,
    };
    attrs = boundResizing(
      unscaledImgDimensions,
      { ...unscaledImgDimensions, x: 0, y: 0 },
      { ...unscaledImgDimensions, abstractX: 0, abstractY: 0 },
      isCustom || isEllipse ? false : properCropRatio,
      cropConfig,
    );
  } else {
    attrs = crop;
  }
  const { x = 0, y = 0, width, height } = attrs;
  return {
    x: isFlippedX ? imageDimensions.width - x - width : x,
    y: isFlippedY ? imageDimensions.height - y - height : y,
    width,
    height,
  };
}

export function getProperCropRatio(cropRatio, originalImage) {
  return cropRatio === ORIGINAL_CROP
    ? originalImage.width / originalImage.height
    : cropRatio;
}
