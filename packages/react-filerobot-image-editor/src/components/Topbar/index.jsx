/** External Dependencies */
import React from 'react';
/** Internal Dependencies */
import { ReactComponent as ImageCanvasIcon } from 'common-assets/images/imageCanvasIcon.svg';
import { ReactComponent as DesignCanvasIcon } from 'common-assets/images/designCanvasIcon.svg';
import './index.css';
import Separator from 'components/common/Separator';
import { usePhoneScreen, useStore } from 'hooks';
import CloseButton from './CloseButton';
import SaveButton from './SaveButton';
import ResetButton from './ResetButton';
import UndoButton from './UndoButton';
import RedoButton from './RedoButton';
import ImageDimensionsAndDisplayToggle from './ImageDimensionsAndDisplayToggle';
import CanvasZooming from './CanvasZooming';
import {
  StyledTopbar,
  StyledFlexCenterAlignedContainer,
  StyledHistoryButtonsWrapper,
} from './Topbar.styled';
import BackButton from './BackButton';
import { ImageOptions } from 'components/tools/Image';
import { NavLink } from 'react-router-dom';

const Topbar = () => {
  const {
    config: { showBackButton, disableZooming },
  } = useStore();

  return (
    <StyledTopbar
      reverseDirection={showBackButton}
      className="FIE_topbar"
      data-topbar
    >
      <div data-ide-controller>
        <SaveButton />
        <StyledHistoryButtonsWrapper className="FIE_topbar-history-buttons">
          <ResetButton margin="0" />
          <UndoButton margin="0" />
          <RedoButton margin="0" />
        </StyledHistoryButtonsWrapper>
      </div>
      <ul data-ide-nav>
        {navLinkDatas.map((data) => (
          <li key={data.id}>
            <NavLink
              to={data.id}
              className={({ isActive, isPending }) =>
                isActive ? 'active' : isPending ? 'pending' : ''
              }
            >
              {data.label}
            </NavLink>
          </li>
        ))}
      </ul>
    </StyledTopbar>
  );
};
export default Topbar;

const navLinkDatas = [
  {
    id: 'image-edit-tool',
    label: (
      <>
        <ImageCanvasIcon data-image-edit-logo />
        편집
      </>
    ),
  },
];
