/** Internal Dependencies */
import getDimensionsMinimalRatio from 'utils/getDimensionsMinimalRatio';

export const SET_CANVAS_SIZE = 'SET_CANVAS_SIZE';

const setCanvasSize = (state, payload) => {
  //캔버스 사이즈가 변경되었을 때만 진행한다.
  if (
    state.canvasWidth === payload.canvasWidth &&
    state.canvasHeight === payload.canvasHeight
  ) {
    return state;
  }

  /** if enabled, it wouldn't increase the image size in the container to be more clear for the user to edit the image....
   * which means if the image is small it would be hard to edit after having this enabled...
   * is should replace payload.canvasWidth & payload.canvasHeight in the code afterwards..
   * also it might cause some improper behavior that needs to be debugged if generated.
   */
  // const restrictedWidth = restrictNumber(
  //   payload.canvasWidth,
  //   0,
  //   state.originalImage.width
  // );

  // const restrictedHeight = restrictNumber(
  //   payload.canvasHeight,
  //   0,
  //   state.originalImage.height
  // );

  // 캔버스의 사이즈는 캔버스 컨테이너의 사이즈와 같다.
  const {
    initialCanvasWidth = payload.canvasWidth,
    initialCanvasHeight = payload.canvasHeight,
  } = state;

  const originalImageInitialScale = getDimensionsMinimalRatio(
    state.initialCanvasWidth,
    state.initialCanvasHeight,
    state.originalImage.width,
    state.originalImage.height,
  );

  // 스캐일이 적용된 이미지 크기
  const originalImageInitialResizedWidth =
    originalImageInitialScale * state.originalImage.width;
  const originalImageInitialResizedHeight =
    originalImageInitialScale * state.originalImage.height;

  let scale = 1;
  //캔버스 사이즈가˚ 바뀌고, 이미지도, 그˚ 바뀐 사이즈에 맞춰서 조정되어야 한다 이때 쓰이는 scale로 scale을 업데이트 해준다.
  if (
    initialCanvasWidth !== payload.canvasWidth ||
    initialCanvasHeight !== payload.canvasHeight
  ) {
    const widthScale = payload.canvasWidth / originalImageInitialResizedWidth;
    const heightScale =
      payload.canvasHeight / originalImageInitialResizedHeight;
    scale = Math.min(widthScale, heightScale);
  }

  return {
    ...state,
    initialCanvasWidth, // 초기
    initialCanvasHeight,
    canvasWidth: payload.canvasWidth, // 현재
    canvasHeight: payload.canvasHeight,
    canvasScale: scale, // 이미지를 캔버스에 맞춰주는 사이즈
  };
};

export default setCanvasSize;
